/*******************************************************************************
 * Copyright (c) 2005-2008 VecTrace (Zingo Andersen) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * bastian	implementation
 *******************************************************************************/
package com.vectrace.MercurialEclipse.commands.extensions.mq;

import java.io.File;
import java.util.List;

import org.eclipse.core.resources.IResource;

import com.vectrace.MercurialEclipse.commands.AbstractClient;
import com.vectrace.MercurialEclipse.commands.AbstractShellCommand;
import com.vectrace.MercurialEclipse.commands.HgCommand;
import com.vectrace.MercurialEclipse.commands.HgCommitClient;
import com.vectrace.MercurialEclipse.exception.HgException;
import com.vectrace.MercurialEclipse.model.HgRoot;

/**
 * @author bastian
 *
 */
public class HgQNewClient extends AbstractClient {
	public static String createNewPatch(HgRoot root, String commitMessage, String user, String date, String patchName) throws HgException {
		return createNewPatch(root, commitMessage, null, user, date, patchName, true);
	}

	public static String createNewPatch(HgRoot root, String commitMessage,
			List<IResource> resources, String user, String date, String patchName)
			throws HgException {
		return createNewPatch(root, commitMessage, resources, user, date, patchName, false);
	}

	private static String createNewPatch(HgRoot root, String commitMessage,
			List<IResource> resources, String user, String date, String patchName, boolean all)
			throws HgException {
		HgCommand command = new HgCommand("qnew", //$NON-NLS-1$
				"Invoking qnew", root, true);
		command.setExecutionRule(new AbstractShellCommand.ExclusiveExecutionRule(root));
		File messageFile = null;

		command.addOptions("--config", "extensions.hgext.mq="); //$NON-NLS-1$ //$NON-NLS-2$

		if (commitMessage != null && commitMessage.length() > 0) {
			messageFile = HgCommitClient.addMessage(command, commitMessage);
		}

		command.addOptions("--git"); //$NON-NLS-1$

		if (user != null && user.length() > 0) {
			command.addOptions("--user", user); //$NON-NLS-1$
		} else {
			command.addOptions("--currentuser"); //$NON-NLS-1$
		}

		if (date != null && date.length() > 0) {
			command.addOptions("--date", date); //$NON-NLS-1$
		} else {
			command.addOptions("--currentdate"); //$NON-NLS-1$
		}

		if (!all) {
			if (resources.isEmpty()) {
				command.addOptions("--exclude", "*");
			} else {
				command.addFiles(resources);
			}
		}

		command.addOptions(patchName);

		try {
			return command.executeToString();
		} finally {
			HgCommitClient.deleteMessage(messageFile);
		}
	}
}
